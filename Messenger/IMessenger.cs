﻿namespace Assets.Scripts.Infrastructure.Messaging
{
    public interface IMessenger
    {
        void Subscribe(object subscriber);
        void Unsubscribe(object subscriber);
        void Publish<T>(T message) where T : IMessage;
    }
}