﻿namespace Assets.Scripts.Infrastructure.Messaging
{
    public interface IHandle<in T> where T : IMessage
    {
        void Handle(T message);
    }
}