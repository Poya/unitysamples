﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts.Infrastructure.Helpers
{
    public class Random : IRandom
    {
        readonly System.Random _random;

        public Random()
        {
            _random = Application.isEditor ? new System.Random(1) : new System.Random();
        }

        public double Double()
        {
            return _random.NextDouble();
        }

        public int Integer(int minVal = int.MinValue, int maxVal = int.MaxValue)
        {
            return _random.Next(minVal, maxVal);
        }

        public T From<T>(ICollection<T> collection)
        {
            var length = collection.Count;
            var index = Integer(0, length);

            return collection.ElementAt(index);
        }

        public T FromEnum<T>() where T : struct, IConvertible
        {
            var values = Enum.GetValues(typeof(T));

            var index = Integer(0, values.Length);
            return (T)values.GetValue(index);
        }
    }
}