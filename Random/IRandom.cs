﻿using System;
using System.Collections.Generic;

namespace Assets.Scripts.Infrastructure.Helpers
{
    public interface IRandom
    {
        double Double();
        int Integer(int minVal = int.MinValue, int maxVal = int.MaxValue);
        T From<T>(ICollection<T> collection);
        T FromEnum<T>() where T : struct, IConvertible;
    }
}